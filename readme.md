## Widget Chat WhatsApp
Want to contact customer service quickly and easily? Just one click you can already connect with our customer service, directly from your WhatsApp.

### Constributors
- [Maks Miliyan](https://www.maksdzgn.com)
- [Nova Ardiansyah](https://www.instagram.com/novaardiansyah._)

### License
- protected by [MIT License](https://github.com/novaardiansyah1/widget-chat-whatsapp/blob/master/LICENSE)

### Demo
- Live Demo [here](https://novaardiansyah1.github.io/widget-chat-whatsapp/)

### Status
- Maintenance

### Social Media
- GitHub [Nova Ardiansyah](https://github.com/novaardiansyah1)
- GitLab [Nova Ardiansyah](https://gitlab.com/novaardiansyah1)
- LinkedIn [Nova Ardiansyah](https://linkedin.com/mwlite/in/novaardiansyah)
- Facebook [Nova Ardiansyah](https://facebook.com/nova981)
- Instagram [@Novaardiansyah._](https://www.instagram.com/novaardiansyah._)